const express = require('express');
const cors = require('cors');

const authRoute = require("./routes/AuthRoute");

const app = express();
const port = 3000;

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.use('/auth', authRoute);

app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`)
})