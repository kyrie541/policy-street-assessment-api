const { secret_key, node_env, bcrypt_salt_rounds } = require('../env');
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const db = require('../models');

exports.loginUser = (req, res) => {
    res.send('login user');
    return;
    db.User.findOne({
        where: {
            username: req.body.username,
        },
    })
        .then(user => {
            bcrypt.compare(req.body.password, user.password).then(response => {
                if (response === true) {
                    const token = jwt.sign({ userId: user.id, username: user.username }, secret_key, {
                        expiresIn: 86400,
                    });
                    // Note: becareful 'secure' property, if url is 'https', 'secure'should be true
                    res.cookie("identity_token", token, { secure: node_env == 'production', httpOnly: true });
                    res.redirect('/users');
                } else {
                    req.session.error = 'Login fail';
                    res.redirect('/admin-login');
                }
            });
        })
        .catch(err => {
            req.session.error = 'Login fail';
            res.redirect('/admin-login');
        });
};

exports.logoutUser = (req, res) => {
    res.clearCookie('identity_token');
    res.redirect('/admin-login');
}

module.exports = exports;