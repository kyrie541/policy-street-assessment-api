const { db_host, db_database, db_username, db_password, db_port, db_test_database } = require('../env');

module.exports = {
    "development": {
        "username": db_username,
        "password": db_password,
        "database": db_database,
        "host": db_host,
        "port": db_port,
        "dialect": "postgres"
    },
    "test": {
        "username": db_username,
        "password": db_password,
        "database": db_test_database,
        "host": db_host,
        "port": db_port,
        "dialect": "postgres",
        "logging": false,
    },
    "production": {
        "username": db_username,
        "password": db_password,
        "database": db_database,
        "host": db_host,
        "port": db_port,
        "dialect": "postgres",
        "logging": false,
    }
}